#include <iostream>
using namespace std;
int main(){
    float input;
    //const int all = 72000000;
    cout << "Enter your ownership in KTB: ";
    cin >> input;
    float percent = input*100;
    if (input < 0){
        cout << "You can't own less than 0%" << endl;
    }
    else if (input > 1){
        cout << "You can't own more than 100%" << endl;
    }
    else{
        cout << "You have " << input*100 << "% shares or " << percent*(72000000/100) << " BGN." << endl;
    }

    return 0;
}
