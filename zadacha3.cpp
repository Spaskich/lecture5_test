#include <iostream>
using namespace std;
int main(){
    string employee;
    int hours;
    float wage;
    cout << "Enter employee: ";
    cin >> employee;
    cout << "Enter his hourly wage: ";
    cin >> wage;
    if (wage <= 0){
        cout << "You can't have negative or no salary!" << endl;
    }
    else{
        cout << "Enter how many hours the employee has worked last week: ";
        cin >> hours;
        if (hours <= 0){
            cout << "You can't work less than one hour." << endl;
        }
        else if (hours > 168){
            cout << "You can't work more than 168 hours a week." << endl;
        }
        else if (hours > 40){
            cout << "The salary of " << employee << " for the last week is " << 40*wage + (hours-40)*(1.5*wage) << " BGN." << endl;
        }
        else{
            cout << "The salary of " << employee << " for the last week is " << hours*wage << " BGN." << endl;
        }
    }

    return 0;
}
